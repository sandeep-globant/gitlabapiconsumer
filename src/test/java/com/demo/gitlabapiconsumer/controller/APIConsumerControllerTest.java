package com.demo.gitlabapiconsumer.controller;


import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslJsonArray;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.core.model.RequestResponsePact;
import au.com.dius.pact.core.model.annotations.Pact;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.gitlab4j.api.models.Member;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "GitLabCatalogue")
public class APIConsumerControllerTest {

    @Autowired
    APIConsumerController apiConsumerController;

    @Pact(consumer = "APIConsumerCatalogue")
    public RequestResponsePact pactGitlabAPI(PactDslWithProvider builder) {
        return builder.given("members exist").
                uponReceiving("getting All memebrs details")
                .path("/getMembers/32568636")
                .willRespondWith().status(200).body(
                        PactDslJsonArray.arrayMinLike(1)
                                .stringType("username", "sandeep-globant").closeObject()).toPact();

    }

    @Test
    @PactTestFor(pactMethod = "pactGitlabAPI", port = "9999")

    public void testgetAllProjectMembers(MockServer mockServer) throws JsonMappingException, JsonProcessingException {

        String expectedJson = "[{\"username\":\"sandeep-globant\"}]";

        apiConsumerController.setApiBaseUrl(mockServer.getUrl());

        List<Member> memberList = apiConsumerController.getAllProjectMembers(32568636);
        ObjectMapper obj = new ObjectMapper();
        String jsonActual = obj.writeValueAsString(memberList);

        Assertions.assertEquals(expectedJson, jsonActual);


    }
}
