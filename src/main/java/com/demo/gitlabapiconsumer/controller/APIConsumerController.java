
package com.demo.gitlabapiconsumer.controller;

import org.gitlab4j.api.models.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class APIConsumerController {

    @Value("${gitlab.api.url}")
    private String apiBaseUrl;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/getProjectMemebrs/{id}")
    public List<Member> getAllProjectMembers(@PathVariable("id") Integer id){
        String url = apiBaseUrl+"/getMembers/"+id;
        System.out.println(url);
        HttpHeaders headers = new HttpHeaders();
        headers.add("PRIVATE-TOKEN", "");
        HttpEntity entity = new HttpEntity(headers);
        List<Member> members = restTemplate.getForEntity(url, List.class).getBody();

        return members;

    }

    public String getApiBaseUrl() {
        return apiBaseUrl;
    }

    public void setApiBaseUrl(String apiBaseUrl) {
        this.apiBaseUrl = apiBaseUrl;
    }
}
